# standard
import time

# third-party
import requests

# user defined
from svsEvents import REC, ARV, LFT

# telegram bot and user info
user_id = 836757491
covid_19_alert_bot = '1461441381:AAF54BUrd5sYJTISc-MPvSCeR8zcIfeZhb0'
ac_bot = '1403384712:AAGMIkvQy7KqHdp4q5yFbFnQvBL4G4PlfAw'
welcome_bot = '1485898967:AAHhq43uOleY0-pU4VN8z_ODQm6Cfqi76RM'

# telegram message function
def TelegramBotSend(bot_api_key, chat_id, text):
	try:
		url = 'https://api.telegram.org/bot{bot_api_key}/'\
				'sendMessage?chat_id={chat_id}'\
				'&text={text}'.format(bot_api_key=bot_api_key,
									 chat_id=chat_id,
									 text=text)

		r = requests.get(url)

		if r.status_code == 200:
			return True
		else:
			print("Request failed. Server returned:",r.status_code)
			return False
	
	except Exception as e:
		print(str(e))
		return False


# welcome bot
def app1(event):
	"""
	Greet whoever comes in to the building
	"""
	if event.type == REC:
		message = 'Welcome {} to the {} !!!'.format(event.name,event.cameraId)
		print(message)
		TelegramBotSend(welcome_bot,user_id,message)



# Ac bot
ac_state = None

def app2(event):
	"""
	Turn on and off the A/C
	"""
	global ac_state
	if event.type in [ARV,LFT]:

		if event.type == ARV:
			count = event.remainCount +1
		else:
			count = event.remainCount

		ac_requirement = count > 0

		# if initial AC state is not known yet
		if ac_state is None:
			if ac_requirement:
				print('turning ac on..')
				TelegramBotSend(ac_bot,user_id,'turning ac on..')
				ac_state = True
				return
			else:
				print('turning ac off')
				TelegramBotSend(ac_bot,user_id,'turning ac off..')
				ac_state = False
				return

		# AC is already on but need to turn off
		elif ac_state and not ac_requirement:
			print('turning ac off')
			TelegramBotSend(ac_bot,user_id,'turning ac off..')
			ac_state = False
			return
		# ac is already off but need to turn on
		elif not ac_state and ac_requirement:
			print('turning ac on..')
			TelegramBotSend(ac_bot,user_id,'turning ac on..')
			ac_state = True
			return



# covid-19 alert bot
peopleLimit = 3
alertInterval = 60
lastAlert = 0

def app3(event):
	"""
	Alert if a room has too many people in it!!!
	"""
	global lastAlert

	if event.type in [ARV,LFT]:

		if event.type == ARV:
			count = event.remainCount +1
		else:
			count = event.remainCount


		if count > peopleLimit:
			if (lastAlert+alertInterval) < time.time():
				message = 'There are {} people in {}. {} Person(s) must vacate the room !!!'.format(count,event.cameraId,count-peopleLimit)
				print(message)
				TelegramBotSend(covid_19_alert_bot,user_id,message)
				lastAlert = time.time()



