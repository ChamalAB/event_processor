# standard
import unittest

# user-defined
from svsEvents import EventLeft, EventArrived, EventRecognition, getEventObj

# create test scenarios
event_a = {'type': 'arrived',
 'cameraid': 'test_area',
 'tempID': 0,
 'time': 1603371493,
 'remain': [],
 'remainCount': 2}

event_b = {'type': 'recognition',
 'cameraid': 'test_area',
 'tempID': 0,
 'name': 'chamal',
 'time': 1603371493}

event_c = {'type': 'left',
 'cameraid': 'test_area',
 'tempID': 0,
 'time': 1603371493,
 'remain': [],
 'remainCount': 2}


class TestEventobjects(unittest.TestCase):

    def test_object_types(self):
        # create objects
        obj_a = getEventObj(event_a)
        obj_b = getEventObj(event_b)
        obj_c = getEventObj(event_c)
        
        # check instance types returned
        self.assertTrue(isinstance(obj_a,EventArrived))
        self.assertTrue(isinstance(obj_b,EventRecognition))
        self.assertTrue(isinstance(obj_c,EventLeft))
        
        
    def test_ObjStates(self):
        # create objects
        obj_a = getEventObj(event_a)
        obj_b = getEventObj(event_b)
        obj_c = getEventObj(event_c)
        
        # check obkect states
        self.assertEqual(obj_a.timeIn, 1603371493)
        self.assertEqual(obj_b.time, 1603371493)
        self.assertEqual(obj_c.timeOut, 1603371493)
            
            
    

if __name__ == '__main__':
    unittest.main()