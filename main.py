from svsEvents import setDBCreds, attachApp, mainloop
from config import DB_CREDS
from apps import app1, app2, app3


setDBCreds(DB_CREDS)
attachApp(app1)
attachApp(app2)
attachApp(app3)
mainloop()