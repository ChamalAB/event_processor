# standard
import json
from collections import deque

# third-party
import mysql.connector

class databaseConnection:
    def __init__(self, **kwargs):
        self.db = mysql.connector.connect(**kwargs)
    
    
    def execute(self,sql,args,fetch=True):
        result = None
        with self.db.cursor(dictionary=True,buffered=True) as cursor:
            cursor.execute(sql,args)
            self.db.commit()
            if fetch:
                result = cursor.fetchall()
        return result
    
    
    def close(self):
        self.db.close()
        
        
    def __del__(self):
        self.close()
        
        
class svsEventProcDB (databaseConnection):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)
        self.queue = deque()
        
        
    def grabEvents(self):
        results = self.execute('SELECT * FROM svs2.event order by idevent ASC;',())
        
        prim_keys = []
        for event in results:
            prim_keys.append(event['idevent'])
            self.queue.append(json.loads(event['stringified']))
        
        if prim_keys:
            # delete entries
            sql = 'DELETE FROM event where idevent in (%s)' % ','.join(['%s' for i in range(len(prim_keys))])
            args = tuple(prim_keys)
            self.execute(sql,args,fetch=False)
    
    
    def getEvent(self):
        try:
            return self.queue.popleft()
        except IndexError:
            return None