# standard
import time
import signal


# third-party


# user defined
from .database import svsEventProcDB


# Event Objects
REC = 'recognition'
ARV = 'arrived'
LFT = 'left'

class baseEvent:
    def __init__(self):
        self.type = None
        self.tempId = None
        self.cameraId = None
        

class EventLeft(baseEvent):
    def __init__(self):
        self.timeOut = None
        self.remain = None
        self.remainCount = None
        
        
class EventArrived(baseEvent):
    def __init__(self):
        self.timeIn = None
        self.remain = None
        self.remainCount = None
        
        
class EventRecognition(baseEvent):
    def __init__(self):
        self.name = None
        self.time = None
        
def getEventObj(Dict):
    ret = None
    if Dict['type'] == REC:
        ret = EventRecognition()
        ret.type = Dict['type']
        ret.tempId= Dict['tempID']
        ret.cameraId = Dict['cameraid']
        
        ret.time = Dict['time']
        ret.name = Dict['name']
        
    elif Dict['type'] == ARV:
        ret = EventArrived()
        ret.type = Dict['type']
        ret.tempId= Dict['tempID']
        ret.cameraId = Dict['cameraid']
        
        ret.timeIn = Dict['time']
        ret.remain = Dict['remain']
        ret.remainCount = Dict['remainCount']
        
    elif Dict['type'] == LFT:
        ret = EventLeft()
        ret.type = Dict['type']
        ret.tempId= Dict['tempID']
        ret.cameraId = Dict['cameraid']
        
        ret.timeOut = Dict['time']
        ret.remain = Dict['remain']
        ret.remainCount = Dict['remainCount']
    else:
        raise Exception("unknown object type received")
        
    return ret

# setup signal handler
mainloop_state = True

def signal_handler(sig, frame):
    print('Pressed Ctrl+C!. Exiting')
    global mainloop_state
    mainloop_state = False

signal.signal(signal.SIGINT, signal_handler)


appList = []
DB_CREDS = None

def setDBCreds(creds):
    global DB_CREDS
    DB_CREDS = creds

def attachApp(app):
    global appList
    appList.append(app)
    
    
def mainloop():
    if not DB_CREDS:
        raise Exception("database credentials not set")
        
    database = svsEventProcDB(**DB_CREDS)
    while mainloop_state:
        database.grabEvents()
        while True:
            event = database.getEvent()
            if not event:
                break
            for app in appList:
                try:
                    app(getEventObj(event))
                except Exceptoion as e:
                    print(str(e))
        time.sleep(0.5)